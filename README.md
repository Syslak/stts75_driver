# Driver til STT75M2F temperatursensoren

Øverst i main.c fila:

```c
#include "STTS75.h"
```

## Initialiseirng av temperatursensoren
```c
STTS75 temp_sensor = {0};
 
STTS75_Init(&temp_sensor, &hi2c2, STTS75_9BIT);


```
 
## Lesing av data

Ved laveste oppløysning er konverteringstida 85ms, så det har ingenting forseg å lese raskere enn dette.
```c
STTS75_Read_Temp(&temp_sensor);
```

## Klargjering av datapakke

```c
    uint8_t data[8] = {0};
    int16_t temp = (int16_t) (temp_sensor->temp_degc * 100);

    data[0] = temp & (0xFFU); 
    data[1] = temp >> 8; 
```
